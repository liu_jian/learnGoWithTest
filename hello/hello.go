package main

import "fmt"

// Hello 出力
func Hello() string {
	return "Hello, world"
}

func main() {
	fmt.Println(Hello())
}
