package main

// Rectangle 長方形
type Rectangle struct {
	Width  float64
	Height float64
}

// RecLength の周長計算
func RecLength(height float64, width float64) float64 {
	return height*2 + width*2
}

// Area 面積計算
func Area(height float64, width float64) float64 {
	return height * width
}

// RecLengthS structの周長計算
func RecLengthS(rectangle Rectangle) float64 {
	return 2 * (rectangle.Width + rectangle.Height)
}

// AreaS structの面積計算
func AreaS(rectangle Rectangle) float64 {
	return rectangle.Width * rectangle.Height
}
