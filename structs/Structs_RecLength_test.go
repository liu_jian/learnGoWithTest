package main

import "testing"

func TestRecLength(t *testing.T) {
	got := RecLength(10.0, 5.0)
	want := 30.0

	if got != want {
		t.Errorf("Got %.2f Want %.2f", got, want)
	}
}

func TestArea(t *testing.T) {
	got := Area(12.0, 6.0)
	want := 72.0
	if got != want {
		t.Errorf("got %.2f want %.2f", got, want)
	}
}

func TestRecLengthS(t *testing.T) {
	rectangle := Rectangle{11.0, 6.0}
	got := RecLengthS(rectangle)
	want := 34.0
	if got != want {
		t.Errorf("got is %.2f, want is %.2f", got, want)
	}
}

func TestAreaS(t *testing.T) {
	rectangle := Rectangle{11.0, 6.0}
	got := AreaS(rectangle)
	want := 66.0
	if got != want {
		t.Errorf("got is %.2f, want is %.2f", got, want)
	}
}
